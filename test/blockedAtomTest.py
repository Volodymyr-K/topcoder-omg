import subprocess
import os.path

if __name__ == '__main__':
    # go through files in dir structure
    formulas = ['C2H6O', 'C2H4O2', 'C3H6O2' ]
    
    for formula in formulas:
        print 'attempting formula', formula
        base_dir = '../data/' + formula
        good_sdf = base_dir + os.path.sep + 'good.sdf'
        out_sdf = base_dir + os.path.sep + 'out.sdf'
        out_blocked_sdf = base_dir + os.path.sep + 'out_blocked.sdf'
        # run the codes
        args = ['java', '-jar', '../build/dist/OMG.jar', '-ec', formula, '-o', out_sdf ]
        subprocess.call(args)
        args = ['java', '-jar', '../build/dist/OMG.jar', '-ec', formula, '-fr', good_sdf, '-o', out_blocked_sdf ]
        subprocess.call(args)
        print ""
