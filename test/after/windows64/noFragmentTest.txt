attempting formula C2H4O
MaxBond for Nitrogen is set to: 5
CCO
C2H4O
molecules 3
Duration: 626 miliseconds

SDF files test:
Reading ../data/C2H4O\out.sdf ...
Reading ../data/C2H4O\truth.sdf ...
GOOD: sdf files match


attempting formula C3H4O3
MaxBond for Nitrogen is set to: 5
CCCOOO
C3H4O3
molecules 152
Duration: 1005 miliseconds

SDF files test:
Reading ../data/C3H4O3\out.sdf ...
Reading ../data/C3H4O3\truth.sdf ...
GOOD: sdf files match


attempting formula C4H6O5
MaxBond for Nitrogen is set to: 5
CCCCOOOOO
C4H6O5
molecules 8070
Duration: 11123 miliseconds

SDF files test:
Reading ../data/C4H6O5\out.sdf ...
Reading ../data/C4H6O5\truth.sdf ...
GOOD: sdf files match


attempting formula C5H10O5
MaxBond for Nitrogen is set to: 5
CCCCCOOOOO
C5H10O5
molecules 18092
Duration: 40294 miliseconds

SDF files test:
Reading ../data/C5H10O5\out.sdf ...
Reading ../data/C5H10O5\truth.sdf ...
GOOD: sdf files match


attempting formula C2H5NO2
MaxBond for Nitrogen is set to: 5
CCNOO
C2H5NO2
molecules 97
Duration: 932 miliseconds

SDF files test:
Reading ../data/C2H5NO2\out.sdf ...
Reading ../data/C2H5NO2\truth.sdf ...
GOOD: sdf files match


attempting formula C4H7NO3
MaxBond for Nitrogen is set to: 5
CCCCNOOO
C4H7NO3
molecules 26530
Duration: 68019 miliseconds

SDF files test:
Reading ../data/C4H7NO3\out.sdf ...
Reading ../data/C4H7NO3\truth.sdf ...
GOOD: sdf files match