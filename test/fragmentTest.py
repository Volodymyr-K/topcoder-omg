import subprocess
import os.path

if __name__ == '__main__':
	# go through files in dir structure
	# for each base dir
	formulas = ['C6H6O', 'C9H9F3', 'C12H9OCl', 'C14H14O' ]

	for formula in formulas:
		print 'attempting formula', formula
		base_dir = '../data/' + formula
		good_sdf = base_dir + os.path.sep + 'good.sdf'
		bad_sdf = base_dir + os.path.sep + 'bad.sdf'
		out_sdf = base_dir + os.path.sep + 'out.sdf'
		# run the codes
		args = ['java', '-jar', '../build/dist/OMG.jar', '-ec', formula, '-fr', good_sdf, '-badlist', bad_sdf, '-o', out_sdf, '-n', '5']
		subprocess.call(args)

		print 'SDF files test:'
		out_lines = []
		if os.path.isfile(out_sdf):
			with open(out_sdf) as f:
				print 'Reading '+ out_sdf + ' ...'
				out_lines = f.readlines()

		truth_sdf = base_dir + os.path.sep + 'truth.sdf'
		truth_lines = []
		if(os.path.isfile(truth_sdf)):
			with open(truth_sdf) as f:
				print 'Reading '+ truth_sdf + ' ...'
				truth_lines = f.readlines()
		out_n_lines = len(out_lines)
		truth_n_lines = len(truth_lines)
		if (out_n_lines!=truth_n_lines) :
			print 'BAD: different number of lines: ' + str(out_n_lines) + ' vs ' + str(truth_n_lines)
		else:
			n = len(out_lines)
			ok = True
			bad_line_id = -1
			for i in range(0, n):
				out_cur = out_lines[i]
				if(out_cur[0:10]=='  CDK     '):
					continue;
				truth_cur = truth_lines[i]
				if(out_cur!=truth_cur):
					ok = False
					break;
			if(ok):
				print 'GOOD: sdf files match'
			else:
				print 'BAD: different line at: ' + str(bad_line_id + 1)
		print ''
		print ''
				
