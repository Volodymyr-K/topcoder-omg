import subprocess
import os.path

if __name__ == '__main__':
    # go through files in dir structure
    formulas = ['NO3H', 'N2O2H2']
    
    for formula in formulas:
        print 'attempting formula', formula
        base_dir = '../data/' + formula
        good_sdf = base_dir + os.path.sep + 'good.sdf'
        out_n5_sdf = base_dir + os.path.sep + 'out_n5.sdf'
        out_n3_sdf = base_dir + os.path.sep + 'out_n3.sdf'
        # run the codes
        args = ['java', '-jar', '../build/dist/OMG.jar', '-ec', formula, '-n', '5', '-o', out_n5_sdf ]
        subprocess.call(args)
        args = ['java', '-jar', '../build/dist/OMG.jar', '-ec', formula, '-n', '3', '-o', out_n3_sdf ]
        subprocess.call(args)
        print ""
