OMG - Open Molecule Generator
 
Copyright 2011 The Netherland Metabolomics Center Team License: GPL v3, see License-gpl-3.txt

Introduction
========

You are currently reading the README file for the OMG Project. This project is hosted under http://sourceforge.net/p/openmg

OMG is an open-source tool for the generation of chemical structures, implemented in the programming language Java(tm).
The library is published under terms of the  standard The GNU General Public License (GPL) v3.
This has implications on what you can do with sources and binaries of the OMG library. 
For details, please refer to the file License-gpl-3.txt, which is provided with this distribution.

PLEASE NOTE: OMG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

System Requirements
========

OMG.jar and OMG_GUI.jar can be built and run in the following OS;

- Ubuntu 64 bits
- Mac OS X 64 bits
- Windows 64 bits (or 32 bits)

Java 6 or later is required.

Using OMG tool
==============

In order to use the OMG tool in your program, you need to run the jar file via command line and provide some arguments.

-ec:  elemental composition of the molecules to be generated.  
-o:   SDF file where to store the molecules.  
-fr:  SDF file containing prescribed one or multiple substructures. In the case of multiple substructures, they have to be non-overlapping. 

Here are some examples of how to run OMG using the command line: 

1. Generating molecules
    1. Generate molecules for the elemental composition C6H6  
       `java -jar OMG.jar -ec C6H6`

    2. Generate molecules for the elemental composition C6H6 and store them in out_C6H6.sdf  
       `java -jar OMG.jar -ec C6H6 -o out_C6H6.sdf`

2. Generating molecules with prescribed substructure(s)
    1. Generate molecules for the elemental composition C2H5NO2 (glycine) using the prescribed substructure in fragment_CO2.sdf  
       `java -jar OMG.jar -ec C2H5NO2 -fr fragment_CO2.sdf -o out_C2H5NO2.sdf`

File structure
==============

1. lib/ : Third-party dependency libraries.
2. src/ : Contains source code of the application and the nauty C library.
3. test/ : Contains sample sdf files that can be used for testing the application.
4. build.xml : Ant build script.
5. build-dependencies.xml : Dependency import file for the ant build script.
6. License-gpl-3.txt : The license file.
7. README.md : This file.

Setup and build
===============

1. Make sure you have Java 6 or later and Ant installed in your system.
2. Execute `ant build` to compile the app. If you just need the final jar file, you can skip this step and go directly to step 3.
3. Execute `ant dist` to package the app. The output jars will be at build/dist/OMG.jar and build/dist/OMG_GUI.jar
4. Executing `ant clean` will delete the build directory and all build artifacts.

Building nauty
===============

The OMG distribution comes with pre-built nauty binaries for Windows, Mac and Ubuntu64 platforms. If needed, you can rebuild them.

1. Make sure you have Java 6 or later, Ant, Visual Studio 2012 (on Windows), cc (on Mac) and gcc (on Unix) compilers installed in your system.
2. Set JAVA\_HOME env vairable to where your JDK is installed, e.g. /usr/lib/jvm/java-6-openjdk-amd64 or /Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home
3. If building for Windows, you need to set various env variables for the Visual Studio compiler and linker.
   The best and easiest way to do it is to run `vcvarsall.bat amd64` (to build for 64 bit) or `vcvarsall.bat x86` (to build for 32 bit).
   You can find the bat file in your Visual Studio distribution, e.g. C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC
4. Execute `ant build_nauty_64` to build the 64 bit nauty binary for your OS. To build the 32 bit version on Windows, run `ant build_nauty_32` (but remember to also run `vcvarsall.bat x86` first).
   You can find the built nauty binary at build/c
5. Repeat the steps above for all OSes and/or platforms that you need.
6. If you need to repackage the OMG with the new nauty binaries, copy them to the lib folder and follow the steps in the "Setup and build" section.

Acknowledgment
==============

The application was written by Julio E. Peironcely  
jpeironcely@gmail.com  
Leiden, Netherlands  
http://juliopeironcely.com
