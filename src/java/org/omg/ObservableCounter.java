package org.omg;

import java.util.Observable;

public class ObservableCounter extends Observable {
    private volatile int count;

    public void reset() {
        count = 0;
        notifyObservers();
    }

    public void increment() {
        count++;
        notifyObservers();
    }

    public int getCount() {
        return count;
    }
}