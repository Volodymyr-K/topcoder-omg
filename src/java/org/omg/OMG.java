package org.omg;

import org.openscience.cdk.ChemFile;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.atomtype.CDKAtomTypeMatcher;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.graph.ConnectivityChecker;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomContainerSet;
import org.openscience.cdk.interfaces.IAtomType;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IBond.Order;
import org.openscience.cdk.interfaces.IChemSequence;
import org.openscience.cdk.io.MDLV2000Reader;
import org.openscience.cdk.io.SDFWriter;
import org.openscience.cdk.silent.SilentChemObjectBuilder;
import org.openscience.cdk.tools.CDKHydrogenAdder;
import org.openscience.cdk.tools.ElementComparator;
import org.openscience.cdk.tools.SaturationChecker;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import org.openscience.cdk.tools.manipulator.AtomTypeManipulator;
import org.openscience.cdk.tools.manipulator.MolecularFormulaManipulator;
import org.openscience.smsd.Substructure;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.Comparator;
import java.util.Collections;

/**
 * Open Molecule Generation
 * The main class collecting parameters and setting global objects
 * 
 * @author Julio Peironcely
 */
public class OMG {
	SDFWriter outFile;
	HashMap<String, Byte> visitedmap;

	public static final ObservableCounter molCounter = new ObservableCounter();
	private static final AtomicBoolean stop = new AtomicBoolean(false);

	private int atom_counter;
	private int maxOpenings;
	private int nH;
	private static boolean wfile = false;
	private static String fragments = null;
    private static String badlist = null;
    private IAtomContainerSet badlistquery = null;
    private CDKHydrogenAdder hAdder;
	private CDKAtomTypeMatcher matcher;
	private SaturationChecker satCheck;
	private static ConnectivityChecker conCheck;
	private static Map<String, Integer> valencetable; 
	static {

		// initialize the table
		valencetable = new HashMap<String, Integer>();
		// TODO: read atom symbols from CDK
		valencetable.put("C", new Integer(4));
		valencetable.put("N", new Integer(5));
		valencetable.put("O", new Integer(2));
		valencetable.put("S", new Integer(6));
		valencetable.put("P", new Integer(5));
		valencetable.put("F", new Integer(1));
		valencetable.put("I", new Integer(1));
		valencetable.put("Cl", new Integer(1));
		valencetable.put("Br", new Integer(1));
	}

	public static void stop() {
		if (stop.compareAndSet(false, true)) {
			System.out.println("Stop processing requested...");
		}
	}

	public static void main(String[] args) throws IOException{

		OMG gen = new OMG();
		String formula = null;
		String out = "default_out.sdf";
		int nMaxBond = 3;

                initializeOptions();
		
		if (args.length > 0) {
			
			for(int i = 0; i < args.length; i++){
				if(args[i].equals("-h")){
					System.out.println("OMG generates chemical structures");
					System.out.println("");
					System.out.println("Usage: java -jar OMG.jar -ec <elemental_composition> [-o <out_file.sdf>, -fr <in_fragments.sdf> -badlist <bad_fragments.sdf> -n <nitrogen_max_bond>]");
					System.out.println("");
					System.out.println("Required Parameters");
					System.out.println("-ec:  elemental composition of the molecules to be generated.");
					System.out.println("");
					System.out.println("Optional Parameters");
					System.out.println("-o:   SDF file where to store the molecules. ");
					System.out.println("-fr:  SDF file containing prescribed one or multiple substructures. In the case");
					System.out.println("         of multiple substructures, they have to be non-overlapping. ");
                    System.out.println("-badlist:  SDF file containing prescribed one or multiple bad substructures to filter. In the case");
                    System.out.println("         of multiple substructures, they have to be non-overlapping. ");
					System.out.println("-n: the maximum number of bond for Nitrogen atom, an integer within [3,5], the default is 3");
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("Examples:");
					System.out.println("java -jar OMG.jar -ec C6H6");
					System.out.println("");
					System.out.println("java -jar OMG.jar -ec C6H6 -o out_C6H6.sdf");
					System.out.println("");
					System.out.println("java -jar OMG.jar -ec C2H5NO2 -fr fragment_CO2.sdf");

                    System.out.println("java -jar OMG.jar -ec C2H5NO2 -badlist fragment_CO2.sdf");
					System.out.println("");

					System.exit(1);
				}
				if(args[i].equals("-ec")){
					try {
						formula = args[i+1];
				    } catch (Exception e) {
				        System.err.println("No formula provided");
				        System.exit(1);
				    }
				}
				else if(args[i].equals("-o")){
					try {
						out = args[i+1];
						wfile = true;
				    } catch (Exception e) {
				        System.err.println("No output file provided");
				        System.exit(1);
				    }
				}
				else if(args[i].equals("-fr")){
					try {
						fragments = args[i+1];
				    } catch (Exception e) {
				        System.err.println("No file with prescribed substructures provided");
				        System.exit(1);
				    }
				}

                else if(args[i].equals("-badlist")){
                    try {
                        badlist = args[i+1];
                    } catch (Exception e) {
                        System.err.println("No file with prescribed bad substructures provided");
                        System.exit(1);
                    }
                }
                else if(args[i].equals("-n")) {
					try {
						nMaxBond = Integer.parseInt(args[i+1]);
						nMaxBond = (nMaxBond<3) ? 3 : ((nMaxBond>5) ? 5 : nMaxBond);
					} catch (Exception e) {
                        System.err.println("MaxBond for Nitrogen error for the number provided");
                        System.exit(1);						
					}
                }
			}
		}
		else{
			System.err.println("Provide at least an elemental composition. Type OMG.jar -h for help");
			System.exit(1);
		}
		try {
			gen.initializeMolecule(formula, fragments, nMaxBond, out);
		} catch (CDKException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public OMG(){
		matcher = CDKAtomTypeMatcher.getInstance(SilentChemObjectBuilder.getInstance());
		hAdder = CDKHydrogenAdder.getInstance(SilentChemObjectBuilder.getInstance());
	}

	static void initializeOptions() {
		molCounter.reset();
		stop.set(false);
		wfile = false;
		fragments = null;
		badlist = null;
	}
	
	/**
	 * Build a IAtomContainer from the specified formula. Atoms are sorted with CDK ElementComparator class.
	 * 
	 * @param formula	a string, chemical formula of the molecule (ex: C12H9OCl)
	 * @return an IAtomContainer which holds the parsed formula description
	 */
	IAtomContainer buildMoleculeFromFormula(String formula) throws CDKException, CloneNotSupportedException {
		IAtomContainer acontainer = MolecularFormulaManipulator.getAtomContainer(
				MolecularFormulaManipulator.getMolecularFormula(formula, DefaultChemObjectBuilder.getInstance()));

		List<IAtom> atoms = new ArrayList<IAtom>();
		for(IAtom atom: acontainer.atoms())
			atoms.add(atom.clone());
		Comparator<IAtom> aComparator = new Comparator<IAtom>() {
			public int compare(IAtom a1, IAtom a2) {
				return (new ElementComparator()).compare(a1.getSymbol(), a2.getSymbol());
			}
		};
		Collections.sort(atoms, aComparator);
		acontainer.removeAllElements();
		for(IAtom atom: atoms)
			acontainer.addAtom(atom);	
		return acontainer;
	}
	
	/**
	 * Read a sdf file containing some fragments, parse it and return an list of IAtomContainer
	 * 
	 * @param fragments a path to a sdf file containing some fragments
	 * @return an ArrayList<IAtomContainer> which holds the parsed fragments description
	 */
	 ArrayList<IAtomContainer> buildFragmentContainers(String fragments) throws CDKException, FileNotFoundException, CloneNotSupportedException {
		InputStream ins = new BufferedInputStream(new FileInputStream(fragments));
		MDLV2000Reader reader = new MDLV2000Reader(ins);
		ChemFile fileContents = (ChemFile)reader.read(new ChemFile());
		// Only one sequence and many ChemModel in the sequence
		IChemSequence sequence = fileContents.getChemSequence(0);
		
		ArrayList<IAtomContainer> fragmentContainers = new ArrayList<IAtomContainer>();
		for (int i=0; i<sequence.getChemModelCount(); i++) {
			// one IAtomContainer in a MoleculeSet
			IAtomContainer frag = sequence.getChemModel(i).getMoleculeSet().getAtomContainer(0);
			fragmentContainers.add(frag);
		}
		return fragmentContainers;
	}
	
	/**
	 * Initialize the molecule from the formula and fragments descriptions
	 * 
	 * @param formula	a string, chemical formula of the molecule (ex: C12H9OCl)
	 * @param fragments	a path to a sdf file containing some fragments
	 * @param nMaxBond	the maximum allowed number of bonds for Nitrogen
	 */
	void initializeMolecule(String formula, String fragments, int nMaxBond, String output) throws CDKException, FileNotFoundException, CloneNotSupportedException {
		long before = System.currentTimeMillis();
		MolManipulator.updateBondTable("N", nMaxBond);
		System.out.println("MaxBond for Nitrogen is set to: " + nMaxBond);
		IAtomContainer acontainer = buildMoleculeFromFormula(formula);
		nH = 0;
		List<IAtom> listcont = new ArrayList<IAtom>();
		atom_counter = 0;
		String symbol = null;
		for(IAtom atom: acontainer.atoms()){
			symbol = atom.getSymbol();
			if(symbol.equals("H")){
				nH++;
				maxOpenings--;
				listcont.add(atom);
			}
			else{
				maxOpenings += valencetable.get(symbol);
				atom.setID("a"+atom_counter);
				atom.setFlag(1, false);
				atom_counter++;
				System.out.print(atom.getSymbol());
			}
		}
		System.out.println();
		for(IAtom atom: listcont){
			acontainer.removeAtom(atom);
		}
		
		if(fragments != null){
			ArrayList<IAtomContainer> fragmentContainers = buildFragmentContainers(fragments);
			System.out.println("Number of fragments: " + fragmentContainers.size());
			for (IAtomContainer frag: fragmentContainers) {
				try {
                    // restrict the atom structure.
					acontainer = MolManipulator.buildFromFragment(acontainer,frag);
				} catch (CloneNotSupportedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			acontainer = MolManipulator.getcanonical(acontainer);
		}

		
        if (badlist != null) {
            InputStream ins = new BufferedInputStream(new FileInputStream(badlist));
            MDLV2000Reader reader = new MDLV2000Reader(ins);
            ChemFile fileContents = (ChemFile)reader.read(new ChemFile());

            IChemSequence sequence = fileContents.getChemSequence(0);
            badlistquery = sequence.getChemModel(0).getMoleculeSet();
            for (int i=1; i<sequence.getChemModelCount(); i++) {
                badlistquery.add(sequence.getChemModel(i).getMoleculeSet());
            }
        }

		molCounter.reset();
		try {
			outFile = new SDFWriter(new FileWriter(output));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {

			satCheck = new SaturationChecker();
			conCheck = new ConnectivityChecker();
			visitedmap = new HashMap<String, Byte>();
			String canstr = MolManipulator.mol2array(acontainer, (atom_counter>4));
			visitedmap.put(canstr, null);
			generateMol(acontainer, null, false);

			if (stop.compareAndSet(true, false)) {
				System.out.println("Stopped. Output is partial.");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long after = System.currentTimeMillis();
		try {
			outFile.close();
			System.out.println(formula);
			System.out.println("molecules " + molCounter.getCount());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Duration: " + (after - before) + " miliseconds\n");
		
	}
	
	/**
	 * Recursive Molecule Generator. The generation steps are:
	 * We check that the atoms are connected in the same molecules. This is, they are not separated fragments.
	 * We add hydrogens in order to check if the molecule is saturated.
	 * We will accept the molecule if the number of hydrogens necessary to saturate is the same as the hydrogens in the original formula.
	 * 
	 * @param acontainer	a IAtomContainer object describing the current molecule
	 * @param canstr2		a string that is a hash representation of the canonical graph
	 * @param extraAtoms	a boolean to force the addition of and extra atom (indeed an extra bond)
	 */
	private void generateMol(IAtomContainer acontainer, String canstr2, boolean extraAtoms) throws CloneNotSupportedException, CDKException, IOException {
		if (stop.get()) {
			return;
		}

		IAtomContainer acprotonate = (IAtomContainer) acontainer.clone();
		int bondCount = 0;
		boolean isComplete = false;
		try {
			for (IAtom atom : acprotonate.atoms()) {
				IAtomType type = matcher.findMatchingAtomType(acprotonate, atom);
				AtomTypeManipulator.configure(atom, type);
			}
			hAdder.addImplicitHydrogens(acprotonate);
			if(AtomContainerManipulator.getTotalHydrogenCount(acprotonate)==nH){
				isComplete = satCheck.isSaturated(acprotonate);
			}
		} catch (IllegalArgumentException iae){
			isComplete = false;
		}
		if(isComplete && !extraAtoms){
			// If the molecule is valid, save it
			if(conCheck.partitionIntoMolecules(acprotonate).getAtomContainerCount() == 1 && checkBad(acprotonate, badlistquery) && checkImplicitH(acprotonate) ){
				molCounter.increment();
				if(wfile){
					acprotonate.setProperty("Id", molCounter.getCount());
					String s = MolManipulator.decompressGraph(canstr2, (atom_counter>4));
					acprotonate.setProperty("can_string", s);
					outFile.write(acprotonate);
				}
			}
			for (IBond b:acprotonate.bonds()) {
				bondCount += b.getOrder().ordinal()+1;
			}
			if (maxOpenings>bondCount*2) {
				generateMol(acontainer, canstr2, true);
			}
		} else {
			// Add or increase an extra bond
			ArrayList<int[]> extBondlist = MolManipulator.extendMol(acontainer);
			for(int[] bond : extBondlist){
				
				IAtomContainer m_ext = (IAtomContainer) acontainer.clone();
				//Only one object bond is used, and recycled at every time we use it
				Order ord = null;
				IBond bondAdd = m_ext.getBond(m_ext.getAtom(bond[0]), m_ext.getAtom(bond[1]));
				if(bondAdd == null){
					m_ext.addBond(bond[0], bond[1], IBond.Order.SINGLE);
				}
				else{
					ord = bondAdd.getOrder();
					if(ord == IBond.Order.SINGLE){
						bondAdd.setOrder(IBond.Order.DOUBLE);
					}
					else if(ord == IBond.Order.DOUBLE){
						bondAdd.setOrder(IBond.Order.TRIPLE);
					}
				}
				// end add bond
				IAtomContainer canonM_ext = MolManipulator.getcanonical(m_ext);
				String canstr =  MolManipulator.mol2array(canonM_ext, (atom_counter>4));
				if(!visitedmap.containsKey(canstr)||(acontainer.getBondCount()==0)) {
					visitedmap.put(canstr, null);
					generateMol(canonM_ext, canstr, false);					
				}
			} // End of extBondlist loop
		} // End of if not complete else
	}

	public int getFinalCount() {
		// TODO Auto-generated method stub
		return molCounter.getCount();
	}

    public static boolean checkBad (IAtomContainer acprotonate, IAtomContainerSet badlistquery) throws CDKException {
        // output only molecules that DON'T contain the substructures in badlistquery
        if(badlistquery != null){
            Substructure substructure = null;
            for(int i = 0; i < badlistquery.getAtomContainerCount(); i++){
                substructure = new Substructure(badlistquery.getAtomContainer(i),acprotonate,true, false, false, false);
                if (substructure.isSubgraph()) {
                    return false;
                }
            }
        }
        return true;
    }
	
	/**
	 * Check if the specified number of minimum implicit H (in fragments) is satisfied.
	 * Return true if all conditions are ok, else false.
	 * 
	 * @param ac	An atom container
	 * @return 		A boolean, the result of the test
	 */
    public static boolean checkImplicitH (IAtomContainer ac) throws CDKException {
		// output only molecules that satisfy the minimum implicit H conditions
        for(IAtom a: ac.atoms()) {
			if(a.getProperty("ImplicitH")!=null) {
				int implicit = a.getImplicitHydrogenCount();
				int requested = a.getProperty("ImplicitH");
				if(requested<implicit) {
					System.out.println("Implicit H error on atom '" + a.getSymbol() + "' : required number of H not reached");
					return false;
				}
			}
		}
        return true;
    }    
    
}
