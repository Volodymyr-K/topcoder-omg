package org.omg;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import java.awt.EventQueue;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class MainPage {
    private JFrame frmParallelMoleculeGenerator;
    private JTextField txtFormula;
    private JTextField txtFragment;
    private JTextField txtBadList;
    private JTextField txtOutput;
    private JTextArea resultArea;
    private JTextField textMolCount;
    private JButton submit;
    private JButton btnStop;
    private Thread omgThread;
    private Thread molCountThread;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MainPage e = new MainPage();
                    e.frmParallelMoleculeGenerator.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }

            }
        });
    }

    public MainPage() {
        initialize();
    }

    private void updateTextArea(final String text) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                MainPage.this.resultArea.append(text);
            }
        });
    }

    private void redirectSystemStreams() {
        OutputStream out = new OutputStream() {
            public void write(int b) throws IOException {
                MainPage.this.updateTextArea(String.valueOf((char)b));
            }

            public void write(byte[] b, int off, int len) throws IOException {
                MainPage.this.updateTextArea(new String(b, off, len));
            }

            public void write(byte[] b) throws IOException {
                write(b, 0, b.length);
            }
        };
        PrintStream printStream = new PrintStream(out, true);
        System.setOut(printStream);
        System.setErr(printStream);
    }

    private void initialize() {
        frmParallelMoleculeGenerator = new JFrame();
        frmParallelMoleculeGenerator.setResizable(false);
        frmParallelMoleculeGenerator.setTitle("Open Molecule Generator");
        frmParallelMoleculeGenerator.setBounds(100, 100, 568, 517 - 160);
        frmParallelMoleculeGenerator.setDefaultCloseOperation(3);
        frmParallelMoleculeGenerator.getContentPane().setLayout((LayoutManager) null);
        redirectSystemStreams();

        int y = 12;

        txtFragment = new JTextField();
        txtFragment.setEditable(false);
        txtFragment.setBounds(197, y - 5, 160, 22);
        frmParallelMoleculeGenerator.getContentPane().add(txtFragment);
        txtFragment.setColumns(10);

        JLabel lblFragment = new JLabel("Prescribed Fragment");
        lblFragment.setToolTipText("A list of non-overlapping fragments in one SDF file to be used as an initial restriction on the molecule generation");
        lblFragment.setBounds(32, y, 153, 15);
        frmParallelMoleculeGenerator.getContentPane().add(lblFragment);
        JButton btnFragment = new JButton("Browse");
        btnFragment.addActionListener(new MainPage.FileOpen(txtFragment));
        btnFragment.setBounds(369, y - 6, 87, 25);
        frmParallelMoleculeGenerator.getContentPane().add(btnFragment);

        JButton button_2 = new JButton("Clear");
        button_2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainPage.this.txtFragment.setText("");
            }
        });
        button_2.setBounds(468, y - 6, 79, 25);
        frmParallelMoleculeGenerator.getContentPane().add(button_2);

        y += 30;

        txtBadList = new JTextField();
        txtBadList.setEditable(false);
        txtBadList.setColumns(10);
        txtBadList.setBounds(197, y - 3, 160, 22);
        frmParallelMoleculeGenerator.getContentPane().add(txtBadList);

        JButton btnBadList = new JButton("Browse");
        btnBadList.addActionListener(new MainPage.FileOpen(txtBadList));
        btnBadList.setBounds(369, y - 4, 87, 25);
        frmParallelMoleculeGenerator.getContentPane().add(btnBadList);
        JLabel lblBadList = new JLabel("Bad list");
        lblBadList.setToolTipText("Postriory constraint on the forbidden substructures");
        lblBadList.setBounds(33, y - 1, 153, 15);
        frmParallelMoleculeGenerator.getContentPane().add(lblBadList);

        JButton button = new JButton("Clear");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainPage.this.txtBadList.setText("");
            }
        });
        button.setBounds(468, y - 4, 79, 25);
        frmParallelMoleculeGenerator.getContentPane().add(button);

        y += 50;

        JButton btnOutput = new JButton("Browse");
        btnOutput.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser choice = new JFileChooser();
                choice.setDialogTitle("Select an SDF file");
                choice.setAcceptAllFileFilterUsed(true);
                choice.setFileFilter(new SdfFilter());

                try {
                    int e1 = choice.showSaveDialog(MainPage.this.frmParallelMoleculeGenerator);
                    if (e1 == 0) {
                        String fragmentFileName = choice.getSelectedFile().getCanonicalPath();
                        if (!fragmentFileName.endsWith(".sdf")) {
                            fragmentFileName = fragmentFileName + ".sdf";
                        }

                        MainPage.this.txtOutput.setText(fragmentFileName);
                    }
                } catch (IOException var5) {
                    var5.printStackTrace();
                }

            }
        });
        btnOutput.setBounds(369, y - 5, 87, 25);
        frmParallelMoleculeGenerator.getContentPane().add(btnOutput);
        JLabel lblSaveOutputAs = new JLabel("Save output as");
        lblSaveOutputAs.setToolTipText("if not specified, only the number of molecules is reported");
        lblSaveOutputAs.setBounds(33, y, 153, 15);
        frmParallelMoleculeGenerator.getContentPane().add(lblSaveOutputAs);

        txtOutput = new JTextField();
        txtOutput.setEditable(false);
        txtOutput.setColumns(10);
        txtOutput.setBounds(197, y - 4, 160, 22);
        frmParallelMoleculeGenerator.getContentPane().add(txtOutput);

        JButton btnClear = new JButton("Clear");
        btnClear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainPage.this.txtOutput.setText("");
            }
        });
        btnClear.setBounds(468, y - 5, 79, 25);
        frmParallelMoleculeGenerator.getContentPane().add(btnClear);

        JLabel formulaLabel = new JLabel("Elemental Composition");
        formulaLabel.setBounds(32, 302 - 160, 200, 15);
        frmParallelMoleculeGenerator.getContentPane().add(formulaLabel);
        txtFormula = new JTextField();
        formulaLabel.setLabelFor(txtFormula);
        txtFormula.setBounds(197, 300 - 163, 114, 22);
        frmParallelMoleculeGenerator.getContentPane().add(txtFormula);
        txtFormula.setColumns(10);

        submit = new JButton("Start");
        submit.setBounds(319, 297 - 160, 114, 25);
        frmParallelMoleculeGenerator.getContentPane().add(submit);
        frmParallelMoleculeGenerator.getRootPane().setDefaultButton(submit);

        resultArea = new JTextArea();
        resultArea.setText("Execution results...");
        JScrollPane jsp = new JScrollPane(resultArea);
        jsp.setBounds(32, 330 - 160, 515, 120);
        frmParallelMoleculeGenerator.getContentPane().add(jsp);

        JLabel lblMoleculeCount = new JLabel("Molecule count");
        lblMoleculeCount.setBounds(32, 457 - 160, 122, 15);
        frmParallelMoleculeGenerator.getContentPane().add(lblMoleculeCount);
        textMolCount = new JTextField();
        textMolCount.setEditable(false);
        lblMoleculeCount.setLabelFor(textMolCount);
        textMolCount.setBounds(172, 457 - 163, 114, 22);
        frmParallelMoleculeGenerator.getContentPane().add(textMolCount);
        textMolCount.setColumns(10);

        btnStop = new JButton("Stop");
        btnStop.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainPage.this.resultArea.append("Interrupting execution...\n");
                OMG.stop();

                MainPage.this.btnStop.setEnabled(false);
                MainPage.this.submit.setEnabled(true);
            }
        });
        btnStop.setEnabled(false);
        btnStop.setBounds(438, 297 - 160, 109, 25);
        frmParallelMoleculeGenerator.getContentPane().add(btnStop);

        submit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    MainPage.this.submit.setEnabled(false);
                    final List<String> npe = new ArrayList<String>();

                    String formula = MainPage.this.txtFormula.getText();
                    if (formula.equals("")) {
                        showError("Please provide an elemental composition.");
                        MainPage.this.submit.setEnabled(true);
                        return;
                    }
                    npe.add("-ec");
                    npe.add(formula);

                    String fragment = MainPage.this.txtFragment.getText();
                    if (!fragment.equals("")) {
                        npe.add("-fr");
                        npe.add(fragment);
                    }

                    String badlist = MainPage.this.txtBadList.getText();
                    if (!badlist.equals("")) {
                        npe.add("-badlist");
                        npe.add(badlist);
                    }

                    String out = MainPage.this.txtOutput.getText();
                    if (!out.equals("")) {
                        npe.add("-o");
                        npe.add(out);
                    }

                    MainPage.this.resultArea.setText("");
                    MainPage.this.omgThread = new Thread(new Runnable() {
                        public void run() {
                            try {
                                OMG.main(npe.toArray(new String[npe.size()]));
                            } catch (Exception var5) {
                                System.err.println("Exception!");
                                MainPage.this.resultArea.append(var5.getMessage());
                            } finally {
                                MainPage.this.submit.setEnabled(true);
                                MainPage.this.btnStop.setEnabled(false);
                            }

                        }
                    });
                    MainPage.this.omgThread.start();
                    MainPage.this.molCountThread = new Thread(new Runnable() {
                        public void run() {
                            while(!MainPage.this.submit.isEnabled()) {
                                try {
                                    Thread.sleep(100L);
                                    MainPage.this.textMolCount.setText("" + OMG.molCounter.getCount());
                                } catch (InterruptedException var5) {
                                    var5.printStackTrace();
                                } finally {
                                    MainPage.this.textMolCount.setText("" + OMG.molCounter.getCount());
                                }
                            }

                        }
                    });
                    MainPage.this.molCountThread.start();
                    MainPage.this.btnStop.setEnabled(true);
                } catch (NullPointerException var4) {
                    showError("Null pointer");
                    var4.printStackTrace();
                    MainPage.this.submit.setEnabled(true);
                    MainPage.this.btnStop.setEnabled(false);
                }

            }

            private void showError(String message) {
                JOptionPane.showMessageDialog(MainPage.this.frmParallelMoleculeGenerator, message);
            }
        });
    }

    private final class FileOpen implements ActionListener {
        private JTextField txtField;

        FileOpen(JTextField txtField) {
            this.txtField = txtField;
        }

        public void actionPerformed(ActionEvent e) {
            JFileChooser choice = new JFileChooser();
            choice.setDialogTitle("Select an SDF file");
            choice.setAcceptAllFileFilterUsed(true);
            choice.setFileFilter(new SdfFilter());

            try {
                int e1 = choice.showOpenDialog(MainPage.this.frmParallelMoleculeGenerator);
                if(e1 == 0) {
                    String fragmentFileName = choice.getSelectedFile().getCanonicalPath();
                    txtField.setText(fragmentFileName);
                }
            } catch (IOException var5) {
                var5.printStackTrace();
            }

        }
    }

    private final class SdfFilter extends FileFilter {
        private SdfFilter() {
        }

        public boolean accept(File file) {
            if(file.isDirectory()) {
                return true;
            } else {
                String fileName = file.toString().toLowerCase();
                return fileName.endsWith(".sdf");
            }
        }

        public String getDescription() {
            return "*.sdf";
        }
    }
}