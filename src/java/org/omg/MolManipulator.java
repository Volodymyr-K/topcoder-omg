package org.omg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.tools.manipulator.BondManipulator;


public class MolManipulator {

		/**
		 * Copy and sort an IAtomContainer in canonical order
		 * 
		 * @param ac	original IAtomContainer
		 * @return	a new sorted IAtomContainer
		 * 
		 */
	   public static IAtomContainer getcanonical(IAtomContainer ac) throws CloneNotSupportedException{

		   IAtomContainer ac2 = ac.getBuilder().newInstance(IAtomContainer.class);
			int vCount = ac.getAtomCount();
			int arr1[] = new int[vCount*vCount]; 
			int lab[] = new int[vCount];
			int lab1[] = new int[vCount];
			int ptn[] = new int[vCount];
		
			IBond bond = null;
			for (int i=0; i<vCount; i++){
				lab[i] = i;
				if(i == vCount-1){
					ptn[i]=0;
				}
				else 
				{
					if(ac.getAtom(i).getSymbol().equals(ac.getAtom(i+1).getSymbol())){
						ptn[i]=1;
					}
					else{
						ptn[i]=0;
					}
				}
				bond = null;
				for (int j=0; j<i; j++){
					bond = ac.getBond(ac.getAtom(i), ac.getAtom(j));
					if(bond == null){}
					else{
						int ord = bond.getOrder().ordinal() + 1;
						arr1[i*vCount+j] = ord;
						arr1[i+j*vCount] = ord;
					}
				}
			}
			int ret[] = OMGJNI.getcanmultig(vCount, arr1, lab, ptn);
			
			for (int i = 0; i < vCount;i++){
				lab1[i] = ret[(vCount*vCount)+i];
				IAtom a1 = ac.getAtom(lab1[i]);
				ac2.addAtom(ac.getAtom(lab1[i]));
				if(a1.getProperty("ImplicitH")!=null) {
					IAtom a2 = ac2.getLastAtom();
					a2.setProperty("ImplicitH", a1.getProperty("ImplicitH"));
				}
			}
			for (int i=0; i<vCount; i++){
				for (int j=i+1; j<vCount; j++){
					if(ret[i*vCount+j]>0 || ret[j*vCount+i]>0){
						ac2.addBond(i, j,BondManipulator.createBondOrder(ret[i*vCount+j]));
						/* NOT USED
						if((fragments != null)){
							if(ac.getBond(ac.getAtom(lab1[i]), ac.getAtom(lab1[j])).getProperty("BondINfrag")!= null){
								ac2.getBond(ac2.getAtom(i), ac2.getAtom(j)).setProperty("BondINfrag", ac.getBond(ac.getAtom(lab1[i]), ac.getAtom(lab1[j])).getProperty("BondINfrag"));
							}
						}
						*/
					}
				}
			}

			return ac2;
		}
	
	   /* NOT USED
	   public static ArrayList<IAtomContainer> getcanonical_list(ArrayList<IAtomContainer> extMollist) throws CloneNotSupportedException{

		   int molcount = extMollist.size();
		
			int vCount = extMollist.get(0).getAtomCount();

			int arr1[] = new int[molcount*vCount*vCount]; 
			int rr1[] = new int[vCount*vCount];
			int lab[] = new int[molcount*vCount];
			int lab1[] = new int[vCount];
			int ptn[] = new int[molcount*vCount];
			int count = 0;
			
			int c = 0;
			for(IAtomContainer mol : extMollist){
				for (int i=0; i<vCount; i++){
					lab[(count*vCount)+i] = i;
					if(i == vCount-1){
						ptn[(count*vCount)+i]=0;
					}
					else 
					{
						if(mol.getAtom(i).getSymbol().equals(mol.getAtom(i+1).getSymbol())){
							ptn[(count*vCount)+i]=1;
						}
						else{
							ptn[(count*vCount)+i]=0;
						}
					}
					for (int j=0; j<vCount; j++){
						IBond bond = mol.getBond(mol.getAtom(i), mol.getAtom(j));
						if(bond == null){}
						else if(bond.getOrder() == IBond.Order.SINGLE){
							arr1[c] = 1;
						}
						else if(bond.getOrder() == IBond.Order.DOUBLE){
							arr1[c] = 2;
						}
						else if(bond.getOrder() == IBond.Order.TRIPLE){
							arr1[c] = 3;
						}
						else if(bond.getOrder() == IBond.Order.QUADRUPLE){
							arr1[c] = 4;
						}
						c++;
					}
					
				}
				count++;
			}
			int ret[] = OMGJNI.getcanmultig2(molcount,vCount, arr1, lab, ptn);

			ArrayList<IAtomContainer> listreturn = new ArrayList<IAtomContainer>();
			for(int mc = 0; mc < molcount;mc++){
				IAtomContainer ac2 = extMollist.get(mc).getBuilder().newInstance(IAtomContainer.class);

			
				System.arraycopy(ret, (mc*((vCount*vCount)+vCount)), rr1, 0, (vCount*vCount));
				for (int i = 0; i < vCount;i++){
					
					lab1[i] = ret[((((vCount*vCount)+vCount)*mc)+(vCount*vCount))+i];
					ac2.addAtom(extMollist.get(mc).getAtom(lab1[i]));
				}
	
				for (int i=0; i<vCount; i++){
					for (int j=i+1; j<vCount; j++){
						if(rr1[i*vCount+j]==1){
							ac2.addBond(i, j, IBond.Order.SINGLE);
							if(extMollist.get(mc).getBond(extMollist.get(mc).getAtom(lab1[i]), extMollist.get(mc).getAtom(lab1[j])).getProperty("BondINfrag")!= null){
								ac2.getBond(ac2.getAtom(i), ac2.getAtom(j)).setProperty("BondINfrag", extMollist.get(mc).getBond(extMollist.get(mc).getAtom(lab1[i]), extMollist.get(mc).getAtom(lab1[j])).getProperty("BondINfrag"));
							}
						}	
						else if(rr1[i*vCount+j]==2){
							ac2.addBond(i, j, IBond.Order.DOUBLE);
							if(extMollist.get(mc).getBond(extMollist.get(mc).getAtom(lab1[i]), extMollist.get(mc).getAtom(lab1[j])).getProperty("BondINfrag")!= null){
								ac2.getBond(ac2.getAtom(i), ac2.getAtom(j)).setProperty("BondINfrag", extMollist.get(mc).getBond(extMollist.get(mc).getAtom(lab1[i]), extMollist.get(mc).getAtom(lab1[j])).getProperty("BondINfrag"));
							}
						}
						else if(rr1[i*vCount+j]==3){
							ac2.addBond(i, j, IBond.Order.TRIPLE);
							if(extMollist.get(mc).getBond(extMollist.get(mc).getAtom(lab1[i]), extMollist.get(mc).getAtom(lab1[j])).getProperty("BondINfrag")!= null){
								ac2.getBond(ac2.getAtom(i), ac2.getAtom(j)).setProperty("BondINfrag", extMollist.get(mc).getBond(extMollist.get(mc).getAtom(lab1[i]), extMollist.get(mc).getAtom(lab1[j])).getProperty("BondINfrag"));
							}
						}
						else if(rr1[i*vCount+j]==4){
							ac2.addBond(i, j, IBond.Order.QUADRUPLE);
							if(extMollist.get(mc).getBond(extMollist.get(mc).getAtom(lab1[i]), extMollist.get(mc).getAtom(lab1[j])).getProperty("BondINfrag")!= null){
								ac2.getBond(ac2.getAtom(i), ac2.getAtom(j)).setProperty("BondINfrag", extMollist.get(mc).getBond(extMollist.get(mc).getAtom(lab1[i]), extMollist.get(mc).getAtom(lab1[j])).getProperty("BondINfrag"));
							}
						}
						
					}
				}
				listreturn.add(ac2);
			}	

			return listreturn;
		}
		*/

/**
 * Search for possible molecule extensions. 
 * Browse the molecule using DFS and check new bond possibilities with every other atoms.
 * 
 * @param ac 	IAtomContainer to be browsed
 * @return a list of ids (integer) of the possible new bond
 */
public static  ArrayList<int[]> extendMol(IAtomContainer ac) throws CloneNotSupportedException, CDKException {

	int n = ac.getAtomCount();
	int [] bondCounts = new int [n];
	boolean[] visited = new boolean[n];
	boolean[] processed = new boolean[n];
	boolean[] adj = new boolean[n*n];
	boolean[] selected = new boolean[n*n];
	
	// build adjacency matrix
	for (IBond b:ac.bonds()) {
		int i = ac.getAtomNumber(b.getAtom(0));
		int j = ac.getAtomNumber(b.getAtom(1));
		int k = b.getOrder().ordinal()+1;
		bondCounts[i] += k;
		bondCounts[j] += k;
		adj[i*n+j] = true;
		adj[j*n+i] = true;
	}
	
	// implicit H conditions
	for (IAtom a: ac.atoms()) {
		int i = ac.getAtomNumber(a);
		if(a.getProperty("ImplicitH")!=null)
			bondCounts[i] += (int)a.getProperty("ImplicitH");
		// change bondCounts to be the number of possible new bonds
		bondCounts[i] = maxBondTable.get(a.getSymbol()) - bondCounts[i];
	}
	
	// Stack for DFS
	Stack<Integer> stack = new Stack<Integer>();
	stack.push(0);
	visited[0] = true;
	
	// Depth First Search on the current molecules starting at atom[0]
	ArrayList<int[]> bondList = new ArrayList<int[]>();
	while(!stack.empty()) {
		int i = stack.peek();
		processed[i] = true;
		stack.pop();
		// For the current atom check all possible new bond addition
		if (bondCounts[i]==0) {
		} else {
			for (int j = 0; j < n; j++) {
				if ( processed[j] || (bondCounts[j]==0) ) {
				} else {
					int k = (i<j) ? (i*n+j) : (j*n+i);
					selected[k] = true;
				}
			}
		}	
		// visit neighbors
		for(int j=0; j<n; ++j) if(!visited[j] && adj[i*n+j]) {
			visited[j] = true;
			stack.push(j);
		}
	}
	
	// Add only the selected bonds
	for(int i=0; i<n; ++i)
		for(int j=i+1; j<n; ++j)
			if(selected[i*n+j])
				bondList.add(new int[]{i, j});
	
	return bondList;
}

	/**
	 * Build the adjacency matrix representation of the graph of the molecule.
	 * 
	 * @param ac	IAtomContainer which defines the molecule to be analyzed.
	 * @param dense	a boolean, true for a high compression, false for a low compression
	 * @return 		a string, compressed representation of the adjacency matrix
	 */
	public static String mol2array(IAtomContainer ac, boolean dense) {
		int aCount = ac.getAtomCount();
		int n = aCount*aCount;
		int[] ar = new int[n];
		for(IBond bond : ac.bonds()){
			//we read only the bonds and store the bond degree as ar[i*aCount+j]
			int i = ac.getAtomNumber(bond.getAtom(0));
			int j = ac.getAtomNumber(bond.getAtom(1));
			int ord = bond.getOrder().ordinal()+1;
			// only store the upper matrix
			if(i<j)
				ar[i * aCount + j] = ord;
			else
				ar[j * aCount + i] = ord;
		}
		String res = compressGraph(ar, n, dense);
		return res;
	}
	
	/**
	 * Compress the adjacency matrix of the graph.
	 * the coefficient of the matrix are within [0,3] and can fit into 2 bits.
	 * For 'dense' compression, put 8 coefficients into a char, else just put 1 coefficient into a char.
	 * Currently the dense compression is only invertible for n>4.
	 * Possible improvement: store only the upper matrix (graph is undirected)
	 * 
	 * @param ar	an array of integer which holds the adjacency matrix of the graph
	 * @param n		an number of vertices in the graph
	 * @param dense	a boolean, true for a high compression, false for a low compression
	 * @return 		a string, compressed representation of the adjacency matrix
	 */
	public static String compressGraph(int[] ar, int n, boolean dense) {
		if(!dense) {
			// Use char instead of int (16 bits vs 32 bits), and it is easy to build a String from it.
			char[] res = new char[n];
			for(int i=0; i<n; ++i)
				res[i] = (char)ar[i];
			return new String(res);	
		} else {
			// All coefficient are within [0, 3], hence can be encoded with 2 bits.
			// In Java, a char = 16 bits, so put 8 coefficients within each char
			int n8 = n/8;
			int m = (n-1)/8 + 1;
			char[] res = new char[m];
			int k = 0;
			for(int i=0; i<n8; ++i) {
				res[i] = (char)(
					(ar[k]<<14) + (ar[k+1]<<12) + (ar[k+2]<<10) + (ar[k+3]<<8) + 
					(ar[k+4]<<6) + (ar[k+5]<<4) + (ar[k+6]<<2) + (ar[k+7])
					);
				k += 8;
			}
			if(k<n){
				int last = 0;
				int shift = 14;
				while(k<n) {
					last += (ar[k]<<shift);
					shift -= 2;
					++k;
				}
				res[m-1] = (char)last;
			}
			return new String(res);	
		}		
	}
	
	/**
	 * Decompress a graph representation processed with compressGraph.
	 * It must use the same 'dense' parameter.
	 * 
	 * @param g		a string, compressed representation of the adjacency matrix
	 * @param dense	a boolean, true for a high compression, false for a low compression
	 * @return		a string, standard representation of the adjacency matrix
	 */
	public static String decompressGraph(String g, boolean dense) {
		if(!dense) {
			// Decompress the string (read each char as if it is an integer)
			int n = g.length();
			StringBuilder b = new StringBuilder();	   
			for(int i = 0;i<n; i++)
				b.append((int)g.charAt(i));	
			return b.toString();		   
		} else {
			// The matrix is compacted x8 (see compressGraph).
			// retrieve the original matrix size aCount and for each char fill 8 coefficients
			int aCount = (int)(Math.sqrt(8*g.length()));
			int n = aCount * aCount;
			int n8 = n/8;
			int k = 0;
			StringBuilder b = new StringBuilder();
			for(int i=0; i<n8; ++i) {
				int cur = g.charAt(i);
				int shift = 14;
				for(int j=0; j<8; ++j) {
					int tmp = (cur>>shift);
					b.append(tmp);
					cur %= (1<<shift);
					shift -= 2;
				}
				k += 8;
			}
			if(k<n) {
				int cur = g.charAt(n8);
				int shift = 14;
				while(k<n) {
					int tmp = (cur>>shift);
					b.append(tmp);
					cur %= (1<<shift);
					shift -= 2;
					++k;
				}
			}
			return b.toString();		
		}	
	}

	/**
	 * Build the molecule from the given fragments.
	 * H atoms in fragments are erased, to keep a records of specified number of H, a property "ImplicitH" is set.
	 * It allows to keep the original algorithm logic, which works with implicit hydrogens only.
	 * 
	 * @param acontainer	a IAtomContainer which defines the current state of the molecule.
	 * @param frag			a IAtomContainer describing the fragment to be added.
	 * @return 				a new IAtomContainer, molecule which now contains the new fragment.
	 */
	public static IAtomContainer buildFromFragment(IAtomContainer acontainer, IAtomContainer frag) throws CloneNotSupportedException {
		List<IAtom> listcont = new ArrayList<IAtom>();
		for(IAtom atom: frag.atoms()){
			if(atom.getSymbol().equals("H")){
				listcont.add(atom);
			} else {
				int nH = 0;
				for(IAtom atom2: frag.getConnectedAtomsList(atom))
					if(atom2.getSymbol().equals("H"))
						++nH;
				if(nH>0)
					atom.setProperty("ImplicitH", nH);
			}
			if(atom.getProperty("ImplicitH")!=null)
				System.out.println("One atom '" + atom.getSymbol() + "' requires at least " + atom.getProperty("ImplicitH") + " implicit H");
		}
		for(IAtom atom: listcont){
			frag.removeAtomAndConnectedElectronContainers(atom);
		}
		for(IAtom atomF : frag.atoms()){			
				atomF.setFlag(1, false);
		}
		for(IAtom atom : acontainer.atoms()){
			for(IAtom atomF : frag.atoms()){
				if(atomF.getSymbol().equals(atom.getSymbol())&&(!atomF.getFlag(1))&&(!atom.getFlag(1))){
					atomF.setID(atom.getID());

					atomF.setFlag(1, true);
					atom.setFlag(1, true);
					if(atomF.getProperty("ImplicitH")!=null)
						atom.setProperty("ImplicitH", atomF.getProperty("ImplicitH"));					
					break;
				}
			}
		}
		List<IAtom> atomFinal = new ArrayList<IAtom>();
		for(IAtom atomF : frag.atoms()){
			for(IAtom atom : acontainer.atoms()){
				if(atomF.getID().equals(atom.getID())){
					List<IAtom> atomFs = frag.getConnectedAtomsList(atomF);
					for(IAtom atomF2: atomFs){
						if(!atomFinal.contains(atomF2)){
							for(IAtom atom2 : acontainer.atoms()){
								if(atomF2.getID().equals(atom2.getID())){
									atomFinal.add(atomF);
									acontainer.addBond(acontainer.getAtomNumber(atom),acontainer.getAtomNumber(atom2),frag.getBond(atomF, atomF2).getOrder());
									/* NOT USED
									IBond newbond = acontainer.getBond(atom, atom2);
									newbond.setProperty("BondINfrag", ""+acontainer.getBond(atom, atom2).getOrder());
									newbond.setProperty("checked", "yes");
									*/
								}
							}
						
							}
						}
				}
			}
		}

		return acontainer;
	}

	private static Map<String, Integer> maxBondTable; 
	static {

		// initialize the table
		maxBondTable = new HashMap<String, Integer>();
		// TODO: read atom symbols from CDK
		maxBondTable.put("C", new Integer(4));
		maxBondTable.put("N", new Integer(5));
		maxBondTable.put("O", new Integer(2));
		maxBondTable.put("S", new Integer(6));
		maxBondTable.put("P", new Integer(5));
		maxBondTable.put("F", new Integer(1));
		maxBondTable.put("I", new Integer(1));
		maxBondTable.put("Cl", new Integer(1));
		maxBondTable.put("Br", new Integer(1));
	}
	
	public static void updateBondTable(String symbol, int val) {
		maxBondTable.put(symbol, new Integer(val));
	}

}
